package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	entity "gitlab.com/service31/Data/Entity/User"
	module "gitlab.com/service31/Data/Module/User"
	service "gitlab.com/service31/User/Service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var (
	echoEndpoint  = flag.String("endpoint", "localhost:8000", "endpoint")
	serverChannel chan int
)

type server struct {
	module.UnimplementedUserServiceServer
}

func (s *server) Create(ctx context.Context, in *module.CreateUserRequest) (*module.DetailedUserDTO, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	serviceID, err := uuid.FromString(claim.Audience)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "Bad Claim")
	}
	response, err := service.CreateUser(serviceID, in)
	if err != nil {
		return nil, err
	}
	return response.ToDTO(), nil
}

func (s *server) Update(ctx context.Context, in *module.UpdateUserRequest) (*module.DetailedUserDTO, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	serviceID, err := uuid.FromString(claim.Audience)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "Bad Claim")
	}
	ID, err := uuid.FromString(in.GetID())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	response, err := service.UpdateUser(serviceID, ID, in)
	if err != nil {
		return nil, err
	}
	return response.ToDTO(), nil
}

func (s *server) GetUsers(ctx context.Context, in *module.GetUsersRequest) (*module.RepeatedUserSimpleDTO, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	serviceID, err := uuid.FromString(claim.Audience)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "Bad Claim")
	}
	var ids []uuid.UUID
	for _, id := range in.GetID() {
		i, err := uuid.FromString(id)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, fmt.Sprintf("Bad ID: %s", id))
		}
		ids = append(ids, i)
	}
	response, err := service.GetUsers(serviceID, ids)
	if err != nil {
		return nil, err
	}
	var responses *module.RepeatedUserSimpleDTO
	for _, u := range response {
		responses.Results = append(responses.Results, u.ToSimpleDTO())
	}
	return responses, nil
}

func (s *server) GetUsersByService(ctx context.Context, in *module.GetUsersByServiceRequest) (*module.RepeatedUserSimpleDTO, error) {
	serviceID, err := uuid.FromString(in.GetServiceID())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ServiceID")
	}
	response, err := service.GetUsersByService(serviceID)
	if err != nil {
		return nil, err
	}
	var responses *module.RepeatedUserSimpleDTO
	for _, u := range response {
		responses.Results = append(responses.Results, u.ToSimpleDTO())
	}
	return responses, nil
}

func (s *server) GetCurrentUser(ctx context.Context, in *module.Empty) (*module.DetailedUserDTO, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	ID, err := uuid.FromString(claim.Id)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	response, err := service.GetCurrentUser(ID)
	if err != nil {
		return nil, err
	}
	return response.ToDTO(), nil
}

func (s *server) DeleteUser(ctx context.Context, in *module.DeleteUserRequest) (*module.BoolResponse, error) {
	ID, err := uuid.FromString(in.GetID())
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "Bad ID")
	}
	response, err := service.DeleteUser(ID)
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: response}, nil
}

func (s *server) LoginByUsername(ctx context.Context, in *module.LoginByUsernameRequest) (*module.LoginResponse, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	serviceID, err := uuid.FromString(claim.Audience)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "Bad Claim")
	}
	token, expireAt, err := service.LoginByName(serviceID, in.GetUsername(), in.GetPassword())
	if err != nil {
		return nil, err
	}
	return &module.LoginResponse{
		Token:    token,
		ExpireAt: expireAt,
	}, nil
}

func (s *server) LoginByEmail(ctx context.Context, in *module.LoginByEmailRequest) (*module.LoginResponse, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	serviceID, err := uuid.FromString(claim.Audience)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "Bad Claim")
	}
	token, expireAt, err := service.LoginByName(serviceID, in.GetEmail(), in.GetPassword())
	if err != nil {
		return nil, err
	}
	return &module.LoginResponse{
		Token:    token,
		ExpireAt: expireAt,
	}, nil
}

func (s *server) LoginByPhoneNumber(ctx context.Context, in *module.LoginByPhoneNumberRequest) (*module.LoginResponse, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	serviceID, err := uuid.FromString(claim.Audience)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "Bad Claim")
	}
	token, expireAt, err := service.LoginByName(serviceID, in.GetPhoneNumber(), in.GetPassword())
	if err != nil {
		return nil, err
	}
	return &module.LoginResponse{
		Token:    token,
		ExpireAt: expireAt,
	}, nil
}

func (s *server) ServiceLogin(ctx context.Context, in *module.ServiceLoginRequest) (*module.LoginResponse, error) {
	token, expireAt, err := service.SrvcLogin(in.GetUsername(), in.GetPassword())
	if err != nil {
		return nil, err
	}
	return &module.LoginResponse{
		Token:    token,
		ExpireAt: expireAt,
	}, nil
}

func (s *server) Renew(ctx context.Context, in *module.Empty) (*module.LoginResponse, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	token, expireAt, err := service.Renew(claim)
	if err != nil {
		return nil, err
	}
	return &module.LoginResponse{
		Token:    token,
		ExpireAt: expireAt,
	}, nil
}

func (s *server) CreateService(ctx context.Context, in *module.CreateServiceRequest) (*module.LoginResponse, error) {
	token, expireAt, err := service.CreateService(in.GetUsername(), in.GetPassword())
	if err != nil {
		return nil, err
	}
	return &module.LoginResponse{Token: token, ExpireAt: expireAt}, nil
}

func (s *server) UpdateService(ctx context.Context, in *module.UpdateServiceRequest) (*module.BoolResponse, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	serviceID, err := uuid.FromString(claim.Id)
	if err != nil {
		return nil, status.Error(codes.PermissionDenied, "Bad Claim")
	}
	response, err := service.UpdateService(serviceID, in.GetUsername(), in.GetPassword())
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: response}, nil
}

func (s *server) DeleteService(ctx context.Context, in *module.DeleteServiceRequest) (*module.BoolResponse, error) {
	claim := ctx.Value(common.ClaimMetaKey).(*jwt.StandardClaims)
	response, err := service.DeleteService(uuid.FromStringOrNil(claim.Id))
	if err != nil {
		return nil, err
	}
	return &module.BoolResponse{IsSuccess: response}, nil
}

func runGRPCServer() {
	lis, err := net.Listen("tcp", ":8000")
	logger.CheckFatal(err)

	s := grpc.NewServer(
		grpc.StreamInterceptor(
			grpc_middleware.ChainStreamServer(
				grpc_validator.StreamServerInterceptor(),
				service.StreamServerAuthInterceptor(),
				grpc_zap.StreamServerInterceptor(logger.GetLogger()),
			)),
		grpc.UnaryInterceptor(
			grpc_middleware.ChainUnaryServer(
				grpc_validator.UnaryServerInterceptor(),
				service.UnaryServerAuthInterceptor(),
				grpc_zap.UnaryServerInterceptor(logger.GetLogger()),
			)),
	)
	module.RegisterUserServiceServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		logger.Error(fmt.Sprintf("failed to serve: %v", err))
	}

	serverChannel <- 1
}

func serveSwagger(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./Swagger/user.swagger.json")
}

func runRestServer() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := http.NewServeMux()
	gwmux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure()}
	err := module.RegisterUserServiceHandlerFromEndpoint(ctx, gwmux, *echoEndpoint, opts)
	if logger.CheckError(err) {
		serverChannel <- 999
		return
	}
	mux.Handle("/", gwmux)
	mux.HandleFunc("/swagger.json", serveSwagger)
	fs := http.FileServer(http.Dir("Swagger/swagger-ui"))
	mux.Handle("/swagger-ui/", http.StripPrefix("/swagger-ui", fs))

	logger.CheckError(http.ListenAndServe(":80", mux))
	serverChannel <- 2
}

func main() {
	common.Bootstrap()
	common.DB.AutoMigrate(&entity.Service{}, &entity.User{}, &entity.ThirdPartyAuth{})
	serverChannel = make(chan int)
	go runGRPCServer()
	go runRestServer()
	for {
		serverType := <-serverChannel
		if serverType == 1 {
			//GRPC
			logger.Info("Restarting GRPC endpoint")
			go runGRPCServer()
		} else if serverType == 2 {
			//Rest
			logger.Info("Restarting Rest endpoint")
			go runRestServer()
		} else {
			logger.Fatal("Failed to start some endPoint")
		}
	}
}
