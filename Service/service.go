package service

import (
	"context"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-redis/redis/v7"
	"github.com/gogo/status"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	logger "gitlab.com/service31/Common/Logger"
	redisClient "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/User"
	module "gitlab.com/service31/Data/Module/User"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

var key []byte = []byte(os.Getenv("A_KEY"))

//CreateUser create new user
func CreateUser(serviceID uuid.UUID, in *module.CreateUserRequest) (*entity.User, error) {
	request := in.GetRequest()

	if b, err := isUsernameExists(request.GetUsername(), serviceID); b {
		return nil, err
	}

	if b, err := isEmailExists(request.GetEmail(), serviceID); b {
		return nil, err
	}

	if b, err := isPhoneNumberExists(request.GetPhoneNumber(), serviceID); b {
		return nil, err
	}

	user := &entity.User{
		Username:                    request.GetUsername(),
		Password:                    HashAndSalt([]byte(strings.TrimSpace(request.GetPassword()))),
		Gender:                      request.GetGender(),
		PhoneNumber:                 request.GetPhoneNumber(),
		Email:                       request.GetEmail(),
		Avatar:                      strings.TrimSpace(request.GetAvatar()),
		Bio:                         strings.TrimSpace(request.GetBio()),
		Status:                      strings.TrimSpace(request.GetStatus()),
		IsPrivacyPolicyAgreed:       request.GetIsPrivacyPolicyAgreed(),
		IsPhoneNumberVerified:       request.GetIsPhoneNumberVerified(),
		IsActive:                    request.GetIsActive(),
		IsProfilePrivate:            request.GetIsProfilePrivate(),
		IsPhoneNotificationDisabled: request.GetIsPhoneNotificationDisabled(),
		IsEmailNotificationDisabled: request.GetIsEmailNotificationDisabled(),

		ServiceID: serviceID,
	}
	var err error
	user.DOB, err = time.Parse("2006-01-02", request.GetDOB())
	if err != nil {
		logger.Warn(err.Error())
	}

	err = common.DB.Save(user).Scan(&user).Error
	if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to create new user")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.UserChange, user.ID.String()))
	return user, nil
}

//UpdateUser update existing user
func UpdateUser(serviceID, userID uuid.UUID, in *module.UpdateUserRequest) (*entity.User, error) {
	request := in.GetRequest()
	user, err := getUserByService(serviceID, userID)
	if err != nil {
		return nil, err
	}

	if user == nil || user.ID == uuid.Nil {
		return nil, status.Error(codes.NotFound, "")
	}

	user.Username = request.GetUsername()
	user.Password = HashAndSalt([]byte(request.GetPassword()))
	user.Gender = request.GetGender()
	user.PhoneNumber = request.GetPhoneNumber()
	user.Email = request.GetEmail()
	user.Avatar = strings.TrimSpace(request.GetAvatar())
	user.Bio = strings.TrimSpace(request.GetBio())
	user.Status = strings.TrimSpace(request.GetStatus())
	user.IsPrivacyPolicyAgreed = request.GetIsPrivacyPolicyAgreed()
	user.IsPhoneNumberVerified = request.GetIsPhoneNumberVerified()
	user.IsProfilePrivate = request.GetIsProfilePrivate()
	user.IsPhoneNotificationDisabled = request.GetIsPhoneNotificationDisabled()
	user.IsEmailNotificationDisabled = request.GetIsEmailNotificationDisabled()

	if err := common.DB.Save(user).Scan(&user).Error; logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to update user")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.UserChange, user.ID.String()))
	return user, nil
}

//GetUsers get users based on ids
func GetUsers(serviceID uuid.UUID, ids []uuid.UUID) ([]*entity.User, error) {
	return getUsersByIDs(serviceID, ids)
}

//GetUsersByService get users based on serviceID
func GetUsersByService(serviceID uuid.UUID) ([]*entity.User, error) {
	return getUsersByService(serviceID)
}

//GetCurrentUser gets current user
func GetCurrentUser(userID uuid.UUID) (*entity.User, error) {
	user, err := getUserByID(userID)
	if err != nil {
		return nil, err
	}

	if user == nil || user.ID == uuid.Nil {
		return nil, status.Error(codes.NotFound, "")
	}

	return user, nil
}

//DeleteUser deactivate user
func DeleteUser(userID uuid.UUID) (bool, error) {
	user, err := getUserByID(userID)
	if err != nil {
		return false, err
	}

	if user == nil || user.ID == uuid.Nil {
		return false, status.Error(codes.NotFound, "")
	}

	if err = common.DB.Delete(user).Error; logger.CheckError(err) {
		return false, status.Error(codes.Internal, "Failed to delete user")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.UserChange, user.ID.String()))
	return true, nil
}

//LoginByName for user
func LoginByName(serviceID uuid.UUID, username string, password string) (string, int64, error) {
	user, err := getUserByProperty(serviceID, username)
	if err != nil {
		return "", 0, err
	}
	if !ComparePasswords(user.Password, []byte(password)) {
		return "", 0, status.Error(codes.PermissionDenied, "Username/Password is wrong")
	}
	return generateJWTToken(user.ID, serviceID, "User")
}

//LoginByEmail for user
func LoginByEmail(serviceID uuid.UUID, email string, password string) (string, int64, error) {
	user, err := getUserByProperty(serviceID, email)
	if err != nil {
		return "", 0, err
	}

	if !ComparePasswords(user.Password, []byte(password)) {
		return "", 0, status.Error(codes.PermissionDenied, "Email/Password is wrong")
	}
	return generateJWTToken(user.ID, serviceID, "User")
}

//LoginByPhoneNumber for user
func LoginByPhoneNumber(serviceID uuid.UUID, phoneNumber string, password string) (string, int64, error) {
	user, err := getUserByProperty(serviceID, phoneNumber)
	if err != nil {
		return "", 0, err
	}

	if !ComparePasswords(user.Password, []byte(password)) {
		return "", 0, status.Error(codes.PermissionDenied, "Phone Number/Password is wrong")
	}
	return generateJWTToken(user.ID, serviceID, "User")
}

//CreateService create service account
func CreateService(username string, password string) (string, int64, error) {
	if b, err := isServiceUsernameExists(username); b {
		return "", 0, err
	}
	service := &entity.Service{
		Username: username,
		Password: HashAndSalt([]byte(strings.TrimSpace(password))),
	}

	err := common.DB.Save(service).Scan(&service).Error
	if logger.CheckError(err) {
		return "", 0, status.Error(codes.Internal, "Failed to create service account")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ServiceChange, service.ID.String()))
	return generateJWTToken(service.Base.ID, service.Base.ID, "Service")
}

//SrvcLogin for service accounts
func SrvcLogin(username string, password string) (string, int64, error) {
	service := &entity.Service{}

	redisServiceKey, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.UserHashMap, redisClient.RedisMapKeys.GetServiceMapKey(username))
	if err == redis.Nil {
		return "", 0, status.Error(codes.NotFound, "")
	} else if logger.CheckRedisError(err) {
		return "", 0, status.Error(codes.Internal, "Failed to get service")
	}

	err = redisClient.GetRedisValue(redisServiceKey, &service)
	if err == redis.Nil {
		return "", 0, status.Error(codes.NotFound, "")
	} else if logger.CheckRedisError(err) {
		return "", 0, status.Error(codes.Internal, "Failed to get service")
	}

	if !ComparePasswords(service.Password, []byte(password)) {
		return "", 0, status.Error(codes.PermissionDenied, "Username/Password is wrong")
	}
	token, expiresAt, err := generateJWTToken(service.ID, service.ID, "Service")
	if logger.CheckError(err) {
		return "", 0, err
	}
	return token, expiresAt, nil
}

//Renew claim
func Renew(claim *jwt.StandardClaims) (string, int64, error) {
	return generateJWTToken(uuid.FromStringOrNil(claim.Id), uuid.FromStringOrNil(claim.Audience), claim.Subject)
}

//UpdateService update service account
func UpdateService(serviceID uuid.UUID, username string, password string) (bool, error) {
	service, err := getServiceAccountByID(serviceID)
	if err != nil {
		return false, err
	}
	if service.Username != username {
		if b, err := isServiceUsernameExists(username); b {
			return false, err
		}
	}
	service.Username = username
	service.Password = strings.TrimSpace(password)
	err = common.DB.Save(service).Scan(&service).Error
	if logger.CheckError(err) {
		return false, status.Error(codes.Internal, "Failed to update service account")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ServiceChange, service.ID.String()))
	return true, nil
}

//DeleteService deactivate service account
func DeleteService(serviceID uuid.UUID) (bool, error) {
	service, err := getServiceAccountByID(serviceID)
	if err != nil {
		return false, err
	}
	err = common.DB.Delete(service).Scan(&service).Error
	if logger.CheckError(err) {
		return false, status.Error(codes.Internal, "Failed to deactivate service account")
	}
	logger.CheckError(redisClient.PushItemChangedMessage(redisClient.RedisChangeKeys.ServiceChange, service.ID.String()))
	return true, nil
}

//UnaryServerAuthInterceptor user unary interceptor
func UnaryServerAuthInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		newCtx := ctx
		var err error
		if info.FullMethod != "/user.UserService/ServiceLogin" && info.FullMethod != "/user.UserService/CreateService" {
			newCtx, err = common.Authenticate(ctx)
			if err != nil {
				return nil, err
			}
		}
		return handler(newCtx, req)
	}
}

//StreamServerAuthInterceptor user stream interceptor
func StreamServerAuthInterceptor() grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		newCtx := stream.Context()
		var err error
		if info.FullMethod != "/user.UserService/ServiceLogin" {
			newCtx, err = common.Authenticate(stream.Context())
		}
		if err != nil {
			return err
		}
		wrapped := grpc_middleware.WrapServerStream(stream)
		wrapped.WrappedContext = newCtx
		return handler(srv, wrapped)
	}
}

func getUserByID(userID uuid.UUID) (*entity.User, error) {
	user := &entity.User{}
	err := redisClient.GetRedisValue(redisClient.RedisMapKeys.GetUserKey(userID.String()), &user)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckRedisError(err) {
		return nil, status.Error(codes.Internal, "Failed to get user")
	}
	return user, nil
}

func getUserByProperty(serviceID uuid.UUID, property string) (*entity.User, error) {
	user := &entity.User{}
	mapKey, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.UserHashMap, redisClient.RedisMapKeys.GetUserStringPropertyMapKey(serviceID.String(), property))
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckRedisError(err) {
		return nil, status.Error(codes.Internal, "Failed to get user")
	}

	err = redisClient.GetRedisValue(mapKey, &user)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckRedisError(err) {
		return nil, status.Error(codes.Internal, "Failed to get user")
	}
	return user, nil
}

func getUserByService(serviceID, userID uuid.UUID) (*entity.User, error) {
	user := &entity.User{}
	mapKey, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.UserHashMap, redisClient.RedisMapKeys.GetUserByServiceMapKey(serviceID.String(), userID.String()))
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckRedisError(err) {
		return nil, status.Error(codes.Internal, "Failed to get user")
	}

	err = redisClient.GetRedisValue(mapKey, &user)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckRedisError(err) {
		return nil, status.Error(codes.Internal, "Failed to get user")
	}
	return user, nil
}

func getUsersByService(serviceID uuid.UUID) ([]*entity.User, error) {
	var users []*entity.User
	mapKeys, err := redisClient.GetRedisHashScanValues(redisClient.RedisMapKeys.UserHashMap, redisClient.RedisMapKeys.GetUsersByServiceMapKey(serviceID.String()))
	if err == redis.Nil {
		return users, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get users")
	}
	redisUsers, err := redisClient.GetRedisValues(mapKeys)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckRedisError(err) {
		return nil, status.Error(codes.Internal, "Failed to get users")
	}
	for _, v := range redisUsers {
		if v != nil {
			user := &entity.User{}
			err = user.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				users = append(users, user)
			}
		}
	}
	return users, nil
}

func getUsersByIDs(serviceID uuid.UUID, ids []uuid.UUID) ([]*entity.User, error) {
	var userEntities []*entity.User
	var redisKeys []string
	for _, id := range ids {
		redisKeys = append(redisKeys, redisClient.RedisMapKeys.GetUserByServiceMapKey(serviceID.String(), id.String()))
	}
	mapKeys, err := redisClient.GetRedisHashValues(redisClient.RedisMapKeys.UserHashMap, redisKeys)
	if err == redis.Nil {
		return userEntities, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get users")
	}

	redisUsers, err := redisClient.GetRedisValues(mapKeys)
	if err == redis.Nil {
		return userEntities, nil
	} else if logger.CheckError(err) {
		return nil, status.Error(codes.Internal, "Failed to get users")
	}
	for _, v := range redisUsers {
		if v != nil {
			user := &entity.User{}
			err = user.UnmarshalBinary([]byte(fmt.Sprintf("%v", v)))
			if !logger.CheckError(err) {
				userEntities = append(userEntities, user)
			}
		}
	}
	return userEntities, nil
}

func getServiceAccountByID(serviceID uuid.UUID) (*entity.Service, error) {
	service := &entity.Service{}
	err := redisClient.GetRedisValue(redisClient.RedisMapKeys.GetServiceKey(serviceID.String()), &service)
	if err == redis.Nil {
		return nil, status.Error(codes.NotFound, "")
	} else if logger.CheckRedisError(err) {
		return nil, status.Error(codes.Internal, "Failed to get service")
	}
	return service, nil
}

func isServiceUsernameExists(username string) (bool, error) {
	if username == "" {
		return false, nil
	}

	key := redisClient.RedisMapKeys.GetServiceMapKey(username)
	val, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.UserHashMap, key)
	if err == redis.Nil {
		return false, nil
	} else if logger.CheckError(err) {
		return true, status.Error(codes.Internal, "Failed to check username")
	} else if val == "" {
		return false, nil
	}
	return true, status.Error(codes.AlreadyExists, "Username already exists")
}

func isUsernameExists(username string, serviceID uuid.UUID) (bool, error) {
	if username == "" {
		return false, nil
	}

	key := redisClient.RedisMapKeys.GetUserStringPropertyMapKey(serviceID.String(), username)
	val, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.UserHashMap, key)
	if err == redis.Nil {
		return false, nil
	} else if logger.CheckError(err) {
		return true, status.Error(codes.Internal, "Failed to check username")
	} else if val == "" {
		return false, nil
	}
	return true, status.Error(codes.AlreadyExists, "Username already exists")
}

func isEmailExists(email string, serviceID uuid.UUID) (bool, error) {
	if email == "" {
		return false, nil
	}

	key := redisClient.RedisMapKeys.GetUserStringPropertyMapKey(serviceID.String(), email)
	val, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.UserHashMap, key)
	if err == redis.Nil {
		return false, nil
	} else if logger.CheckError(err) {
		return true, status.Error(codes.Internal, "Failed to check phone number")
	} else if val == "" {
		return false, nil
	}
	return true, status.Error(codes.AlreadyExists, "Email already exists")
}

func isPhoneNumberExists(phoneNumber string, serviceID uuid.UUID) (bool, error) {
	if phoneNumber == "" {
		return false, nil
	}

	key := redisClient.RedisMapKeys.GetUserStringPropertyMapKey(serviceID.String(), phoneNumber)
	val, err := redisClient.GetRedisHashValue(redisClient.RedisMapKeys.UserHashMap, key)
	if err == redis.Nil {
		return false, nil
	} else if logger.CheckError(err) {
		return true, status.Error(codes.Internal, "Failed to check phone number")
	} else if val == "" {
		return false, nil
	}
	return true, status.Error(codes.AlreadyExists, "Phone number already exists")
}

func generateJWTToken(ID, serviceID uuid.UUID, subject string) (string, int64, error) {
	claim := &jwt.StandardClaims{
		ExpiresAt: time.Now().Add(30 * time.Minute).Unix(),
		Id:        ID.String(),
		Audience:  serviceID.String(),
		Issuer:    "zh-code.com",
		Subject:   subject,
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	tokenStr, err := token.SignedString(common.JwtKey)
	if logger.CheckError(err) {
		return "", 0, status.Error(codes.Internal, "Failed to generate token")
	}
	return tokenStr, claim.ExpiresAt, nil
}
