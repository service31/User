package service

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	entity "gitlab.com/service31/Data/Entity/User"
)

func TestMain(m *testing.M) {
	SetupTesting()
	common.DB.AutoMigrate(&entity.Service{}, &entity.User{}, &entity.ThirdPartyAuth{})
	defer DoneTesting()
	m.Run()
}

func TestCreateUserUsernameExists(t *testing.T) {
	in, serviceID := GetTestCreateUserUsernameExistsData()

	user, err := CreateUser(serviceID, in)

	if err == nil {
		t.Error("User created with duplicate username")
	}
	if user != nil {
		t.Error("User created with duplicate username, user is not null")
	}
}

func TestCreateUserEmailExists(t *testing.T) {
	in, serviceID := GetTestCreateUserEmailExistsData()

	user, err := CreateUser(serviceID, in)

	if err == nil {
		t.Error("User created with duplicate email")
	}
	if user != nil {
		t.Error("User created with duplicate email, user is not null")
	}
}

func TestCreateUserPhoneNumberExists(t *testing.T) {
	in, serviceID := GetTestCreateUserPhoneNumberExistsData()

	user, err := CreateUser(serviceID, in)

	if err == nil {
		t.Error("User created with duplicate phone number")
	}
	if user != nil {
		t.Error("User created with duplicate phone number, user is not null")
	}
}

func TestCreateUser(t *testing.T) {
	in, serviceID := GetTestCreateUserData()

	user, err := CreateUser(serviceID, in)

	if err != nil {
		t.Errorf("Create user has error %v", err)
	}
	if user == nil {
		t.Error("Create User, user is not null")
	}
}

func TestUpdateUser(t *testing.T) {
	in, serviceID := GetTestCreateUserData()
	user, err := CreateUser(serviceID, in)
	bytes, err := user.MarshalBinary()
	if err != nil {
		t.Error("Failed to marshal user")
	}
	SetTestRedisUser(user, bytes)

	updatedUser, err := UpdateUser(serviceID, user.ID, GetTestUpdateUserData())

	if err != nil {
		t.Errorf("Update user has error %v", err)
	}
	if user.Username == updatedUser.Username {
		t.Errorf("Update user doesn't update username")
	}
}

func TestGetUsers(t *testing.T) {
	var ids []uuid.UUID
	ins, serviceID := GetTestGetUsersData()
	for _, in := range ins {
		user, err := CreateUser(serviceID, in)
		if err != nil {
			t.Error(err)
		}
		bytes, err := user.MarshalBinary()
		if err != nil {
			t.Error("Failed to marshal user")
		}
		SetTestRedisUser(user, bytes)
		ids = append(ids, user.ID)
	}

	users, err := GetUsers(serviceID, ids)
	if err != nil {
		t.Error(err)
	} else if len(users) != len(ins) {
		t.Errorf("TestGetUsers: %d Users inserted but got %d", len(ins), len(users))
	}
}

func TestGetUserByService(t *testing.T) {
	ins, serviceID := GetTestGetUsersData()
	for _, in := range ins {
		user, err := CreateUser(serviceID, in)
		if err != nil {
			t.Error(err)
		}
		bytes, err := user.MarshalBinary()
		if err != nil {
			t.Error("Failed to marshal user")
		}
		SetTestRedisUser(user, bytes)
	}

	users, err := GetUsersByService(serviceID)
	if err != nil {
		t.Error(err)
	} else if len(users) != len(ins) {
		t.Errorf("TestGetUserByService: %d Users inserted but got %d", len(ins), len(users))
	}
}

func TestGetCurrentUser(t *testing.T) {
	in, serviceID := GetTestCreateUserData()
	user, err := CreateUser(serviceID, in)
	bytes, err := user.MarshalBinary()
	if err != nil {
		t.Error("Failed to marshal user")
	}
	SetTestRedisUser(user, bytes)

	u, err := GetCurrentUser(user.ID)

	if err != nil {
		t.Errorf("TestGetCurrentUser has error %v", err)
	}
	if u == nil {
		t.Error("TestGetCurrentUser user is nil")
	}
}

func TestDeleteUser(t *testing.T) {
	in, serviceID := GetTestCreateUserData()
	user, err := CreateUser(serviceID, in)
	bytes, err := user.MarshalBinary()
	if err != nil {
		t.Error("Failed to marshal user")
	}
	SetTestRedisUser(user, bytes)

	u, err := DeleteUser(user.ID)

	if err != nil {
		t.Errorf("TestDeleteUser has error %v", err)
	}
	if !u {
		t.Error("TestDeleteUser response is false")
	}
}

func TestLoginByName(t *testing.T) {
	in, serviceID := GetTestCreateUserData()
	user, err := CreateUser(serviceID, in)
	bytes, err := user.MarshalBinary()
	if err != nil {
		t.Error("Failed to marshal user")
	}
	SetTestRedisUser(user, bytes)

	token, expire, err := LoginByName(serviceID, in.GetRequest().GetUsername(), in.GetRequest().GetPassword())
	if err != nil {
		t.Errorf("TestLoginByName has error %v", err)
	} else if expire < 1 {
		t.Errorf("TestLoginByName expire time < 0")
	} else if len(token) < 1 {
		t.Errorf("TestLoginByName no token returned")
	}
}

func TestLoginByEmail(t *testing.T) {
	in, serviceID := GetTestCreateUserData()
	user, err := CreateUser(serviceID, in)
	bytes, err := user.MarshalBinary()
	if err != nil {
		t.Error("Failed to marshal user")
	}
	SetTestRedisUser(user, bytes)

	token, expire, err := LoginByEmail(serviceID, in.GetRequest().GetEmail(), in.GetRequest().GetPassword())
	if err != nil {
		t.Errorf("TestLoginByEmail has error %v", err)
	} else if expire < 1 {
		t.Errorf("TestLoginByEmail expire time < 0")
	} else if len(token) < 1 {
		t.Errorf("TestLoginByEmail no token returned")
	}
}

func TestLoginByPhoneNumber(t *testing.T) {
	in, serviceID := GetTestCreateUserData()
	user, err := CreateUser(serviceID, in)
	bytes, err := user.MarshalBinary()
	if err != nil {
		t.Error("Failed to marshal user")
	}
	SetTestRedisUser(user, bytes)

	token, expire, err := LoginByPhoneNumber(serviceID, in.GetRequest().GetPhoneNumber(), in.GetRequest().GetPassword())
	if err != nil {
		t.Errorf("TestLoginByPhoneNumber has error %v", err)
	} else if expire < 1 {
		t.Errorf("TestLoginByPhoneNumber expire time < 0")
	} else if len(token) < 1 {
		t.Errorf("TestLoginByPhoneNumber no token returned")
	}
}
