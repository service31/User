package service

import (
	uuid "github.com/satori/go.uuid"
	common "gitlab.com/service31/Common"
	redis "gitlab.com/service31/Common/Redis"
	entity "gitlab.com/service31/Data/Entity/User"
	module "gitlab.com/service31/Data/Module/User"
)

//SetupTesting Init test
func SetupTesting() {
	common.BootstrapTesting(false)
}

//DoneTesting close all connections
func DoneTesting() {
	defer common.DB.Close()
	defer redis.RedisClient.Close()
}

//GetTestCreateUserUsernameExistsData test data
func GetTestCreateUserUsernameExistsData() (*module.CreateUserRequest, uuid.UUID) {
	username := common.GetRandomString(15)
	email := username + "@gmail.com"
	phone := "+1" + common.GetRandomNumberString(10)
	serviceID := uuid.NewV4()
	key := redis.RedisMapKeys.GetUserStringPropertyMapKey(serviceID.String(), username)
	redis.SetRedisHashMapValue(redis.RedisMapKeys.UserHashMap, key, "1")
	m := &module.CreateUserRequest{
		Request: &module.UserRequest{
			Username:    username,
			Email:       email,
			PhoneNumber: phone,
			Password:    common.GetRandomString(15),
			IsActive:    true,
		},
	}
	return m, serviceID
}

//GetTestCreateUserEmailExistsData test data
func GetTestCreateUserEmailExistsData() (*module.CreateUserRequest, uuid.UUID) {
	username := common.GetRandomString(15)
	email := username + "@gmail.com"
	phone := "+1" + common.GetRandomNumberString(10)
	serviceID := uuid.NewV4()
	key := redis.RedisMapKeys.GetUserStringPropertyMapKey(serviceID.String(), email)
	redis.SetRedisHashMapValue(redis.RedisMapKeys.UserHashMap, key, "1")
	m := &module.CreateUserRequest{
		Request: &module.UserRequest{
			Username:    username,
			Email:       email,
			PhoneNumber: phone,
			Password:    common.GetRandomString(15),
			IsActive:    true,
		},
	}
	return m, serviceID
}

//GetTestCreateUserPhoneNumberExistsData test data
func GetTestCreateUserPhoneNumberExistsData() (*module.CreateUserRequest, uuid.UUID) {
	username := common.GetRandomString(15)
	email := username + "@gmail.com"
	phone := "+1" + common.GetRandomNumberString(10)
	serviceID := uuid.NewV4()
	key := redis.RedisMapKeys.GetUserStringPropertyMapKey(serviceID.String(), phone)
	redis.SetRedisHashMapValue(redis.RedisMapKeys.UserHashMap, key, "1")
	m := &module.CreateUserRequest{
		Request: &module.UserRequest{
			Username:    username,
			Email:       email,
			PhoneNumber: phone,
			Password:    common.GetRandomString(15),
			IsActive:    true,
		},
	}
	return m, serviceID
}

//GetTestCreateUserData test data
func GetTestCreateUserData() (*module.CreateUserRequest, uuid.UUID) {
	username := common.GetRandomString(15)
	email := username + "@gmail.com"
	phone := "+1" + common.GetRandomNumberString(10)
	serviceID := uuid.NewV4()
	m := &module.CreateUserRequest{
		Request: &module.UserRequest{
			Username:    username,
			Email:       email,
			PhoneNumber: phone,
			Password:    common.GetRandomString(15),
			IsActive:    true,
		},
	}
	return m, serviceID
}

//GetTestUpdateUserData test data
func GetTestUpdateUserData() *module.UpdateUserRequest {
	username := common.GetRandomString(15)
	email := username + "@gmail.com"
	phone := "+1" + common.GetRandomNumberString(10)
	return &module.UpdateUserRequest{
		Request: &module.UserRequest{
			Username:    username,
			Email:       email,
			PhoneNumber: phone,
			Password:    common.GetRandomString(15),
			IsActive:    true,
		},
	}
}

//GetTestGetUsersData test data
func GetTestGetUsersData() ([]*module.CreateUserRequest, uuid.UUID) {
	serviceID := uuid.NewV4()
	username := common.GetRandomString(15)
	email := username + "@gmail.com"
	phone := "+1" + common.GetRandomNumberString(10)
	var requests []*module.CreateUserRequest
	requests = append(requests, &module.CreateUserRequest{
		Request: &module.UserRequest{
			Username:    username,
			Email:       email,
			PhoneNumber: phone,
			Password:    common.GetRandomString(15),
			IsActive:    true,
		},
	})
	username = common.GetRandomString(15)
	email = username + "@gmail.com"
	phone = "+1" + common.GetRandomNumberString(10)
	requests = append(requests, &module.CreateUserRequest{
		Request: &module.UserRequest{
			Username:    username,
			Email:       email,
			PhoneNumber: phone,
			Password:    common.GetRandomString(15),
			IsActive:    true,
		},
	})
	return requests, serviceID
}

//SetTestRedisUser set user
func SetTestRedisUser(user *entity.User, bytes []byte) {
	key := redis.RedisMapKeys.GetUserKey(user.ID.String())
	serviceMapKey := redis.RedisMapKeys.GetUserByServiceMapKey(user.ServiceID.String(), user.ID.String())
	emailMapKey := redis.RedisMapKeys.GetUserStringPropertyMapKey(user.ServiceID.String(), user.Email)
	usernameMapKey := redis.RedisMapKeys.GetUserStringPropertyMapKey(user.ServiceID.String(), user.Username)
	phoneMapKey := redis.RedisMapKeys.GetUserStringPropertyMapKey(user.ServiceID.String(), user.PhoneNumber)
	redis.SetRedisValue(key, bytes)
	redis.SetRedisHashMapValue(redis.RedisMapKeys.UserHashMap, serviceMapKey, key)
	redis.SetRedisHashMapValue(redis.RedisMapKeys.UserHashMap, emailMapKey, key)
	redis.SetRedisHashMapValue(redis.RedisMapKeys.UserHashMap, usernameMapKey, key)
	redis.SetRedisHashMapValue(redis.RedisMapKeys.UserHashMap, phoneMapKey, key)
}
